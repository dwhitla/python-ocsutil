from random import randrange
from time import sleep
import unittest
from ocsutil.thread import ThreadPool


class ThreadPoolTestCase(unittest.TestCase):
    def test_pool(self):
        delays = [randrange(1, 10) for i in range(100)]

        def wait_delay(seconds):
            print 'sleeping for (%d)sec' % seconds
            sleep(seconds)

        pool = ThreadPool(20)
        for index, delay in enumerate(delays):
            # print the percentage of tasks placed in the queue
            print '%.2f%c' % ((float(index) / float(len(delays))) * 100.0, '%')
            pool.add_task(wait_delay, delay)

        pool.await_completion()


if __name__ == '__main__':
    unittest.main()
