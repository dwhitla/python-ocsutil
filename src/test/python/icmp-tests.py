import unittest
import sys
sys.path.insert(1, '/Users/dwhitla/dev/ocean/projects/python-ocsutil/src/main/python')

from ocsutil.icmp import ping


class PingTestCase(unittest.TestCase):

    def test_ping(self):
        ping("10.160.254.201", count=5)


if __name__ == '__main__':
    unittest.main()
