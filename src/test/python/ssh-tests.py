import unittest
from ocsutil import scp
from ocsutil.scp import SCPException


class SCPTestCase(unittest.TestCase):

    def test_scp_get(self):
        try:
            scp.get("localhost", __file__, "/tmp/")
        except SCPException, e:
            self.fail("Caught SCPException: " + e.message)

    def test_scp_put(self):
        try:
            scp.put("localhost", __file__, "/tmp/foo")
        except SCPException, e:
            self.fail("Caught SCPException: " + e.message)

if __name__ == '__main__':
    unittest.main()
