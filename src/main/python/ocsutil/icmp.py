# noinspection PyPep8Naming
import os
import socket
import struct
from ocsutil.thread import ThreadHandler, ThreadPool
import time
import pwd


# noinspection PyPep8Naming
def ICMPPacket(version, header=(None, None), data=None):
    """
    Create an ICMPv4 or ICMPv6 packet.

    :param version: 4 or 6
    :type version: int
    :param header: two-tuple containing the type and code of the packet.
    :type header: (int, int)
    :param data: Contains data of the packet.  Can only assign a subclass of basestring
        or None.
    :type data: basestring | None
    """
    if version is 4:
        klass = ICMPv4Packet
    elif version is 6:
        klass = ICMPv6Packet
    else:
        raise ValueError("%d is not a valid IP version" % version)
    return klass(header, data)


class ICMPv6Packet(object):

    types = {
        # type: ("description", codes={ code: "description" })
        1: ("Destination Unreachable", {
            0: "No route to destination",
            1: "Communication with destination administratively prohibited",
            2: "Beyond scope of source address",
            3: "Address unreachable",
            4: "Port unreachable",
            5: "Source address failed ingress/egress policy",
            6: "Reject route to destination",
            7: "Error in Source Routing header",
            }),
        2: ("Packet Too Big", {0: "No Code"}),
        3: (6, "Time Exceeded", {
            0: "Hop limit exceeded in transit",
            1: "Fragment reassembly time exceeded",
            }),
        4: ("Parameter Problem", {
            0: "Erroneous header field encountered",
            1: "Unrecognized Next Header type encountered",
            2: "Unrecognized IPv6 option encountered",
            }),
        128: ("Echo Request", {0: "No Code"}),
        129: ("Echo Reply", {0: "No Code"}),
        130: ("Multicast Listener Query", {0: "No Code"}),
        131: ("Multicast Listener Report", {0: "No Code"}),
        132: ("Multicast Listener Done", {0: "No Code"}),
        133: ("Router Solicitation", {0: "No Code"}),
        134: ("Router Advertisement", {0: "No Code"}),
        135: ("Neighbor Solicitation", {0: "No Code"}),
        136: ("Neighbor Advertisement", {0: "No Code"}),
        137: ("Redirect Message", {0: "No Code"}),
        138: ("Router Renumbering", {
            0: "Router Renumbering Command",
            1: "Router Renumbering Result",
            255: "Sequence Number Reset",
            }),
        139: ("ICMP Node Information Query", {
            0: "The Data field contains an IPv6 address which is the Subject of this Query",
            1: "The Data field contains a name which is the Subject of this Query, or is empty, in the case of a NOOP",
            2: "The Data field contains an IPv4 address which is the Subject of this Query",
            }),
        140: ("ICMP Node Information Response", {
            0: "Successful reply",
            1: "The Responder refuses to supply the answer",
            2: "The Qtype of the Query is unknown to the Responder",
            }),
        141: ("Inverse Neighbor Discovery Solicitation Message", {0: "No Code"}),
        142: ("Inverse Neighbor Discovery Advertisement Message", {0: "No Code"}),
        143: ("Multicast Listener Discovery Report", {0: "No Code"}),
        144: ("Home Agent Address Discovery Request Message", {0: "No Code"}),
        145: ("Home Agent Address Discovery Reply Message", {0: "No Code"}),
        146: ("Mobile Prefix Solicitation", {0: "No Code"}),
        147: ("Mobile Prefix Advertisement", {0: "No Code"}),
        148: ("Certification Path Solicitation", {0: "No Code"}),
        149: ("Certification Path Advertisement", {0: "No Code"}),
        151: ("Multicast Router Advertisement", {0: "No Code"}),
        152: ("Multicast Router Solicitation", {0: "No Code"}),
        153: ("Multicast Router Termination", {0: "No Code"}),
        155: ("RPL Control Message", {0: "No Code"}),
        }

    def __init__(self, header=(None, None), data=None):
        """
        Create an ICMPv6 packet.

        :param header: two-tuple containing the type and code of the packet.
        :type header: (int, int)
        :param data: Contains data of the packet.  Can only assign a subclass of basestring
            or None.
        :type data: basestring | None
        """
        self._type = self._code = 0
        self.header = header
        self.data = data


class ICMPv4Packet(object):

    types = {
        # type: ("description", codes={ code: "description" })
        0: ("Echo Reply", {0: "No Code"}),
        3: ("Destination Unreachable", {
            0: "Network Unreachable",
            1: "Host Unreachable",
            2: "Protocol Unreachable",
            3: "Port Unreachable",
            4: "Fragmentation Needed and Don't Fragment was Set",
            5: "Source Route Failed",
            6: "Destination Network Unknown",
            7: "Destination Host Unknown",
            8: "Source Host Isolated",
            9: "Communication with Destination Network is Administratively Prohibited",
            10: "Communication with Destination Host is Administratively Prohibited",
            11: "Destination Network Unreachable for Type of Service",
            12: "Destination Host Unreachable for Type of Service",
            13: "Communication Administratively Prohibited",
            14: "Host Precedence Violation",
            15: "Precedence cutoff in effect",
            }),
        5: ("Redirect", {
            0: "Redirect for Network",
            1: "Redirect for Host",
            2: "Redirect for Type of Service and Network",
            3: "Redirect for Type of Service and Host",
            }),
        6: ("Alternate Host Address", {0: "No Code"}),
        8: ("Echo Request", {0: "No Code"}),
        9: ("Router Advertisement", {0: "No Code"}),
        10: ("Router Selection", {0: "No Code"}),
        11: ("Time Exceeded", {
            0: "TTL Exceeded In Transit",
            1: "Fragment Reassembly Time Exceeded",
            }),
        12: ("Parameter Problem", {
            0: "Pointer indicates the error",
            1: "Missing a Required Option",
            2: "Bad Length",
            }),
        13: ("Timestamp", {0: "No Code"}),
        14: ("Timestamp Reply", {0: "No Code"}),
        }

    def __init__(self, header=(None, None), data=None):
        """
        Create an ICMPv4 packet.

        :param header: two-tuple containing the type and code of the packet.
        :type header: (int, int)
        :param data: Contains data of the packet.  Can only assign a subclass of basestring
            or None.
        :type data: basestring | None
        """
        self.ttl = 64
        self._type = self._code = 0
        self.header = header
        self.data = data

    # noinspection PyAttributeOutsideInit
    def _setheader(self, header):
        if len(header) != 2:
            raise ValueError("header must be a two-tuple")
        type_, code = header
        try:
            desc, codes = self.types[type_]
        except KeyError:
            raise ValueError("%d is not a valid ICMPv4 type" % type_)
        if code not in codes:
            raise ValueError("%d is not a valid code value for '%s' packets" % (code, desc))
        self._type, self._code = type_, code
        self.length = 28

    # noinspection PyAttributeOutsideInit
    def _setdata(self, data):
        if not isinstance(data, basestring) and not isinstance(data, type(None)):
            raise TypeError("value must be a subclass of basestring or None, not %s" % type(data))
        self._data = data
        self.length = max(56, 28 + (len(data) if data else 0))

    def __repr__(self):
        return "<ICMPv4 packet: type = %s, code = %s, data length = %s>" % (self._type, self._code, len(self._data))

    @staticmethod
    def _checksum(packet):
        """Calculate checksum"""
        total = 0

        # Add up 16-bit words
        num_words = len(packet) / 2
        for chunk in struct.unpack("!%sH" % num_words, packet[0:num_words*2]):
            total += chunk

        # Add any left over byte
        if len(packet) % 2:
            total += ord(packet[-1]) << 8

        # Fold 32-bits into 16-bits
        # Note the offset: in C this would return as a uint16_t type, but Python
        # returns it as signed which puts it in the wrong range for struct.pack's H cast
        # Adding the 0xffff offset moves it into the correct range, while the mask removes any overflow.
        total = (total >> 16) + (total & 0xffff)
        total += total >> 16
        return ~total + 0x10000 & 0xffff

    def pack(self):
        """Return a packet's wire representation."""
        # Kept as a separate method instead of rolling into 'packet' property to
        # allow passing method around without having to define a lambda method.
        args = [self._type, self._code, 0]
        pack_format = "!BBH"
        padding_bytes = 36
        if self._data:
            data_length = len(self._data)
            padding_bytes = max(padding_bytes - data_length, 0)
            pack_format += "%ss" % data_length
            args.append(self._data)
        if padding_bytes:
            pack_format += "%dx" % padding_bytes
        args[2] = self._checksum(struct.pack(pack_format, *args))
        return struct.pack(pack_format, *args)

    header = property(lambda self: (self._type, self._code), _setheader, doc="type and code of packet")
    data = property(lambda self: self._data, _setdata, doc="data contained within the packet")

    @classmethod
    def parse(cls, packet):
        """Parse ICMP packet and return an instance of ICMPv4Packet"""
        try:
            version, tos, length, ident, flags, ttl, proto, checksum, srcaddr, dstaddr = struct.unpack(
                "!BBHHHBBHII", packet[:20])
            payload = packet[20:]
            pack_format = "!BBH"
            if len(payload) > 4:
                pack_format += "%ds" % (len(payload) - 4)
            unpacked_payload = struct.unpack(pack_format, payload)
            type_, code, checksum = unpacked_payload[:3]
            try:
                data = unpacked_payload[3]
            except IndexError:
                data = None
            pkt = cls((type_, code), data)
            pkt.srcaddr = srcaddr
            pkt.dstaddr = dstaddr
            pkt.ttl = ttl
            pkt.length = length
            return pkt
        except:
            raise Exception("malformed packet")



def ping(addr, timeout=1, count=1, wait=1, interval=1, ttl=64):

    def _oneping(_ident, _seq):
        packet = ICMPv4Packet((8, 0))
        data = struct.pack("!HHd", _ident, _seq, time.time())
        packet.data = data
        sock.send(packet.pack())
        try:
            buf = sock.recv(576)
            current_time = time.time()
            packet = ICMPv4Packet.parse(buf)
            _ident_, _seq_, timestamp = struct.unpack("!HHd", packet.data[:12])
            if not _ident_ == _ident:
                print "That's odd - we received a pong for someone else's ping"
                return
            rtt = (current_time - timestamp) * 1000
            print "%d bytes from %s: icmp_seq=%u ttl=%d time=%.3f ms" % (len(buf), addr, _seq_, packet.ttl, rtt)
        except socket.timeout:
            print "Request timeout for icmp_seq %d" % _seq

    sock = None
    try:
        sock = socket.socket(socket.AF_INET, socket.SOCK_RAW, socket.IPPROTO_ICMP)
        sock.setsockopt(socket.SOL_IP, socket.IP_TTL, ttl)
        sock.settimeout(timeout)
        sock.connect((addr, 22))
        # setuid back to normal user
        os.setuid(pwd.getpwnam(os.getlogin()).pw_uid)
        ident = os.getpid() % 0x10000

        workers = ThreadPool(count)
        for seq in range(0, count):
            if seq > 0:
                time.sleep(interval)
            workers.add_task(_oneping, None, ident, seq)
        workers.await_completion()
    finally:
        if sock:
            sock.close()
