""" util """
import functools
import re


__all__ = [
    "console", "file", "network", "process", "scp", "ssh", "thread", "vcs", "split_names", "glob_to_regex",
    "as_list", "as_tuple", "si_units", "AttributedDict", "AttributedString", "ContextDecorator"
]


def split_names(name_list):
    names = []
    for name in name_list:
        names.extend(name.split(r" +"))
    return names


def glob_to_regex(text):
    for pattern, replacement in (
            (r"\.", r"\."),
            (r"\*", r".*"),
            (r"\?", r".")
    ):
        text = re.sub(pattern, replacement, text)
    return text


def as_list(option, sep=',', chars=None):
    """Return a list from a ConfigParser option. By default, split on a comma and strip whitespace."""
    return [chunk.strip(chars) for chunk in option.split(sep)]


def as_tuple(line, sep=':'):
    return tuple([e.strip() for e in line.split(sep, 1)])


def si_units(size):
    for prefix, multiplier in (("P", 1073741824.0), ("T", 1048576.0), ("G", 1024.0)):
        if size >= multiplier:
            return "%#.2f %sB" % (size / multiplier, prefix)
    return "%i MB" % size


class AttributedDict(dict):
    def __getattr__(self, key):
        try:
            return self[key]
        except KeyError:
            # to conform with __getattr__ spec
            raise AttributeError(key)

    def __setattr__(self, key, value):
        self[key] = value

    def first(self, *names):
        for name in names:
            value = self.get(name)
            if value:
                return value


class AttributedString(str):
    """
    Simple string subclass to allow arbitrary attribute access.
    """

    @property
    def stdout(self):
        return str(self)


class ContextDecorator(object):
    def __call__(self, func):
        @functools.wraps(func)
        def decorated(*args, **kwargs):
            with self:
                return func(*args, **kwargs)
        return decorated
