import base64
from collections import Iterable
import getpass
from select import select
import socket
import sys
import time
import os
import re

from paramiko.agent import AgentRequestHandler
import paramiko
from ocsutil import AttributedDict, console, AttributedString
from ocsutil.thread import ThreadHandler


MAX_CONNECTION_ATTEMPTS = 5
DEFAULT_SSH_PORT = 22
DEFAULT_CONNECT_TIMEOUT = 5
ipv6_regex = re.compile('^\[?(?P<host>[0-9A-Fa-f:]+)\]?(:(?P<port>\d+))?$')

passwords = {}
user_passwords = {}
_config = None
pkey = None

__all__ = [
    "pkey", "RSAKey", "SSHError", "connect", "call", "execute", "disconnect_all"
]


class SSHError(Exception):
    def __init__(self, *args, **kwargs):
        super(SSHError, self).__init__(*args, **kwargs)


class RSAKey(paramiko.RSAKey):
    def __init__(self, key):
        super(RSAKey, self).__init__()
        self._decode_key(base64.decodestring(key))


def default_user():
    try:
        return getpass.getuser()
    except:
        return "root"


def get_password(user, host):
    return passwords.get("%s@%s" % (user, host), user_passwords.get(user))


def set_password(user, host, password):
    user_passwords[user] = passwords["%s@%s" % (user, host)] = password


def normalized_host_string(user, host, port=None):
    """
    Converts (user, host, port) tuple into ``user@host:port`` string.
    If ``host`` looks like IPv6 address, it will be enclosed in square brackets
    """
    if port is None:
        port = DEFAULT_SSH_PORT
        # Square brackets are necessary for IPv6 host/port separation
    template = "%s@[%s]:%s" if host.count(':') > 1 else "%s@%s:%s"
    return template % (user, host, port)


def normalize(host_string):
    """
    Normalizes a given host string, returning explicit host, user, port.
    """
    if not host_string:
        return '', '', ''
    r = parse_host_string(host_string)
    host = r['host']
    conf = ssh_config(host)
    user = conf['user'] if 'user' in conf else default_user()
    port = conf['port'] if 'port' in conf else DEFAULT_SSH_PORT
    if 'hostname' in conf:
        host = conf['hostname']
    user = r['user'] or user
    port = r['port'] or port
    return user, host, port


def normalize_to_string(host_string):
    return normalized_host_string(*normalize(host_string))


def key_filenames(host):
    """
    Returns list of SSH key filenames for host.

    """
    conf = ssh_config(host)
    keys = []
    if 'identityfile' in conf:
        keys.extend(conf['identityfile'])
    return map(os.path.expanduser, keys)


def _password_prompt(prompt, stream):
    # NOTE: Using encode-to-ascii to prevent (Windows, at least) getpass from choking if given Unicode.
    return getpass.getpass(prompt.encode('ascii', 'ignore'), stream)


def prompt_for_password(user, host, prompt=None, stream=None):
    """
    Prompts for and returns a new password if required; otherwise, returns None.

    A trailing colon is appended. If the user supplies an empty password, the user will be re-prompted until they
    enter a non-empty password.

    """
    _stream = stream or sys.stderr
    password_prompt = prompt if (prompt is not None) else "[%s] Login password for '%s': " % (host, user)
    new_password = getpass.getpass(password_prompt.encode('ascii', 'ignore'), _stream)
    while not new_password:
        print("Sorry, you can't enter an empty password. Please try again.")
        new_password = getpass.getpass(password_prompt.encode('ascii', 'ignore'), _stream)
    return new_password


def connect(user, host, port, timeout=None, keepalive=None, debug=False, sock=None):
    """
    Create and return a new SSHClient instance connected to given host.
    If ``sock`` is given, it's passed into ``SSHClient.connect()`` directly.
    Used for gateway connections by e.g. ``HostConnectionCache``.
    """
    global pkey
    client = paramiko.SSHClient()
    client.load_system_host_keys()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())

    connected = False
    password = get_password(user, host)
    tries = 0
    try:
        keys = key_filenames(host)
    except IOError, e:
        raise SSHError("Your SSH config refers to a private key file '%s' that could not be found." % e.filename)

    while not connected:
        try:
            tries += 1
            client.connect(
                hostname=host,
                port=int(port),
                username=user,
                pkey=pkey,
                password=password,
                key_filename=keys,
                timeout=timeout,
                sock=sock
            )
            connected = True
            if keepalive:
                client.get_transport().set_keepalive(keepalive)
            return client
        # BadHostKeyException corresponds to key mismatch, i.e. what on the
        # command line results in the big banner error about man-in-the-middle
        # attacks.
        except paramiko.BadHostKeyException, e:
            raise SSHError(
                "Host key for %s did not match pre-existing key! Server's key was changed recently, "
                "or possible man-in-the-middle attack." % host, e)
        # Prompt for new password to try on auth failure
        except (paramiko.AuthenticationException, paramiko.PasswordRequiredException, paramiko.SSHException), e:
            msg = str(e)
            # For whatever reason, empty password + no ssh key or agent results in an SSHException instead of an
            # AuthenticationException. Since it's difficult to do otherwise, we must assume
            # empty password + SSHException == auth exception.
            # Conversely: if we get SSHException and there *was* a password -- it is probably something non auth
            # related, and should be sent upwards.
            #
            # This also holds true for rejected/unknown host keys: we have to
            # guess based on other heuristics.
            if e.__class__ is paramiko.SSHException and (password or msg.startswith('Unknown server')):
                raise SSHError(msg, e)

            # Otherwise, assume an auth exception, and prompt for new/better password.

            # Paramiko doesn't handle prompting for locked private keys
            # (i.e. keys with a passphrase and not loaded into an agent)
            # so we have to detect this and tweak our prompt slightly.
            # (Otherwise, however, the logic flow is the same, because
            # ssh's connect() method overrides the password argument to
            # be either the login password OR the private key passphrase. Meh.)
            #
            # NOTE: This will come up if you normally use a passphrase-protected private key with ssh-agent, and enter
            # an incorrect remote username, because ssh.connect:
            # * Tries the agent first, which will fail as you gave the wrong username, so obviously any loaded keys
            #   aren't gonna work for a nonexistent remote account;
            # * Then tries the on-disk key file, which is passphrased;
            # * Realizes there's no password to try unlocking that key with, because you didn't enter a password,
            #   because you're using ssh-agent;
            # * In this condition (trying a key file, password is None) ssh raises PasswordRequiredException.
            text = None
            if e.__class__ is paramiko.PasswordRequiredException:
                # NOTE: we can't easily say WHICH key's passphrase is needed,
                # because ssh doesn't provide us with that info, and
                # env.key_filename may be a list of keys, so we can't know
                # which one raised the exception. Best not to try.
                prompt = "[%s] Passphrase for private key"
                text = prompt % host
            password = prompt_for_password(user, host, text)
            # Update env.password, env.passwords if empty
            set_password(user, host, password)
        # Ctrl-D / Ctrl-C for exit
        except (EOFError, TypeError):
            # Print a newline (in case user was sitting at prompt)
            print('')
            sys.exit(0)
        # Handle DNS error / name lookup failure
        except socket.gaierror, e:
            raise SSHError('Name lookup failed for %s' % host, e)
        # Handle timeouts and retries, including generic errors
        # NOTE: In 2.6, socket.error subclasses IOError
        except socket.error, e:
            not_timeout = type(e) is not socket.timeout
            giving_up = tries >= MAX_CONNECTION_ATTEMPTS
            # Baseline error msg for when debug is off
            msg = "Timed out trying to connect to %s" % host
            # Expanded for debug on
            err = msg + " (attempt %s of %s)" % (tries, MAX_CONNECTION_ATTEMPTS)
            if giving_up:
                err += ", giving up"
            err += ")"
            if debug:
                sys.stderr.write(err + '\n')
                # Having said our piece, try again
            if not giving_up:
                # Sleep if it wasn't a timeout, so we still get timeout-like behavior
                if not_timeout:
                    time.sleep(0 if timeout is None else long(timeout))
                continue
                # Override eror msg if we were retrying other errors
            if not_timeout:
                msg = "Low level socket error connecting to host %s on port %s: %s" % (host, port, e[1])
            msg += " (tried %s time%s)" % (MAX_CONNECTION_ATTEMPTS, "s" if MAX_CONNECTION_ATTEMPTS > 1 else "")
            raise SSHError(msg, e)


class HostConnectionCache(dict):
    """
    This subclass will intelligently create new client connections when keys
    are requested, or return previously created connections instead.

    It also handles creating new socket-like objects when required to implement
    gateway connections and `ProxyCommand`, and handing them to the inner
    connection methods.

    Key values are the same as host specifiers throughout Fabric: optional
    username + ``@``, mandatory hostname, optional ``:`` + port number.
    Examples:

    * ``example.com`` - typical Internet host address.
    * ``firewall`` - atypical, but still legal, local host address.
    * ``user@example.com`` - with specific username attached.
    * ``bob@smith.org:222`` - with specific nonstandard port attached.

    When the username is not given, ``env.user`` is used. ``env.user``
    defaults to the currently running user at startup but may be overwritten by
    user code or by specifying a command-line flag.

    Note that differing explicit usernames for the same hostname will result in
    multiple client connections being made. For example, specifying
    ``user1@example.com`` will create a connection to ``example.com``, logged
    in as ``user1``; later specifying ``user2@example.com`` will create a new,
    2nd connection as ``user2``.

    The same applies to ports: specifying two different ports will result in
    two different connections to the same host being made. If no port is given,
    22 is assumed, so ``example.com`` is equivalent to ``example.com:22``.
    """

    def __init__(self, **kwargs):
        super(HostConnectionCache, self).__init__(**kwargs)

    def __getitem__(self, key):
        return dict.__getitem__(self, normalize_to_string(key))

    def __setitem__(self, key, value):
        return dict.__setitem__(self, normalize_to_string(key), value)

    def __delitem__(self, key):
        return dict.__delitem__(self, normalize_to_string(key))

    def __contains__(self, key):
        return dict.__contains__(self, normalize_to_string(key))


connections = HostConnectionCache()


def _pty_size():
    """
    Obtain (rows, cols) tuple for sizing a pty on the remote end.

    Defaults to 80x24 (which is also the 'ssh' lib's default) but will detect
    local (stdout-based) terminal window size on non-Windows platforms.
    """
    import fcntl
    import termios
    import struct

    rows, cols = 24, 80
    if sys.stdout.isatty():
        # We want two short unsigned integers (rows, cols)
        fmt = 'HH'
        # Create an empty (zeroed) buffer for ioctl to map onto. Yay for C!
        buffer_ = struct.pack(fmt, 0, 0)
        # Call TIOCGWINSZ to get window size of stdout, returns our filled buffer
        try:
            result = fcntl.ioctl(sys.stdout.fileno(), termios.TIOCGWINSZ, buffer_)
            # Unpack buffer back into Python data types
            rows, cols = struct.unpack(fmt, result)
        # Deal with e.g. sys.stdout being monkeypatched, such as in testing.
        # Or termios not having a TIOCGWINSZ.
        except AttributeError:
            pass
    return rows, cols


def parse_host_string(host_string):
    # Split host_string to user (optional) and host/port
    user_hostport = host_string.rsplit('@', 1)
    hostport = user_hostport.pop()
    user = user_hostport[0] if user_hostport and user_hostport[0] else None

    # Split host/port string to host and optional port
    # For IPv6 addresses square brackets are mandatory for host/port separation
    if hostport.count(':') > 1:
        # Looks like IPv6 address
        r = ipv6_regex.match(hostport).groupdict()
        host = r['host'] or None
        port = r['port'] or None
    else:
        # Hostname or IPv4 address
        host_port = hostport.rsplit(':', 1)
        host = host_port.pop(0) or None
        port = host_port[0] if host_port and host_port[0] else None

    return AttributedDict({'user': user, 'host': host, 'port': port})


_ssh_config = None


# noinspection PyTypeChecker
def ssh_config(host):
    """
    Return ssh configuration dict for the supplied host name.
    """
    global _ssh_config
    if _ssh_config is None:
        config_path = os.path.expanduser("~/.ssh/config")
        try:
            config = paramiko.SSHConfig()
            with open(config_path) as fd:
                config.parse(fd)
                _ssh_config = config
        except IOError:
            console.abort("Unable to load SSH config file '%s'" % config_path)
    return _ssh_config.lookup(host)


def call(hosts, func, *args, **kwargs):
    parallel = kwargs.pop("parallel", True)
    if not isinstance(hosts, Iterable):
        hosts = [hosts]
        parallel = False
    if parallel:
        workers = set()
        for host in hosts:
            _args = [host]
            _args.extend(args)
            workers.add(ThreadHandler(func, *_args, **kwargs))
        for worker in workers:
            worker.join()
            worker.raise_if_needed()
        return map(lambda w: w.result(), workers)
    else:
        results = []
        for host in hosts:
            _args = [host]
            _args.extend(args)
            results.append(func(*_args, **kwargs))


def execute(host_string, command, pty=False, combine_stderr=False,
            invoke_shell=False, stdout=None, stderr=None, debug=False, timeout=None):
    """
    Execute ``command`` over ``channel``.

    ``pty`` controls whether a pseudo-terminal is created.
    ``combine_stderr`` controls whether we call ``channel.set_combine_stderr``.
    ``invoke_shell`` controls whether we use ``exec_command`` or
    ``invoke_shell`` (plus a handful of other things, such as always forcing a pty.)

    Returns a three-tuple of (``stdout``, ``stderr``, ``status``), where
    ``stdout``/``stderr`` are captured output strings and ``status`` is the
    program's return code, if applicable.
    """
    # stdout/stderr redirection
    stdout = stdout or sys.stdout
    stderr = stderr or sys.stderr

    user, host, port = normalize(host_string)
    connection = connections.get(host_string, None)
    if connection is None:
        sock = None
        proxy_command = ssh_config(host).get('proxycommand', None)
        if proxy_command:
            sock = paramiko.ProxyCommand(proxy_command)
        connection = connect(user, host, port, timeout=timeout if timeout else DEFAULT_CONNECT_TIMEOUT, sock=sock)
        connections[host_string] = connection
    channel = connection.get_transport().open_session()
    # if no timeout is passed all channel operations are blocking
    channel.settimeout(float(timeout/10) if timeout else None)
    channel.input_enabled = True
    channel.set_combine_stderr(combine_stderr)

    # Assume pty use, and allow overriding of this either via kwarg
    # (invoke_shell always wants a pty no matter what.)
    using_pty = True
    if not invoke_shell and not pty:
        using_pty = False
    if using_pty:
        rows, cols = _pty_size()
        channel.get_pty(width=cols, height=rows)

    # Use SSH agent forwarding from 'ssh' if enabled
    forward_agent = ssh_config(host).get('forwardagent', 'no').lower() == 'yes'
    forward = None
    if forward_agent:
        forward = AgentRequestHandler(channel)

    # Kick off remote command
    if invoke_shell:
        channel.invoke_shell()
        if command:
            channel.sendall(command + "\n")
    else:
        channel.exec_command(command)

    # Init stdout, stderr capturing. Must use lists instead of strings as
    # strings are immutable and we're using these as pass-by-reference
    stdout_buf, stderr_buf = [], []
    if invoke_shell:
        stdout_buf = stderr_buf = None

    workers = (
        ThreadHandler(output_loop, user, host, channel, "recv", stdout, stdout_buf),
        ThreadHandler(output_loop, user, host, channel, "recv_stderr", stderr, stderr_buf),
        ThreadHandler(input_loop, channel, using_pty, debug)
    )

    while True:
        if channel.exit_status_ready():
            break
        else:
            # Check for thread exceptions here so we can raise ASAP
            # (without chance of getting blocked by, or hidden by an exception within, recv_exit_status())
            for worker in workers:
                worker.raise_if_needed()
        time.sleep(paramiko.io_sleep)

    # Obtain exit code of remote program now that we're done.
    status = channel.recv_exit_status()

    # Wait for threads to exit so we aren't left with stale threads
    for worker in workers:
        worker.join()
        worker.raise_if_needed()

    # Close channel
    channel.close()
    # Close any agent forward proxies
    if forward is not None:
        forward.close()

    # Update stdout/stderr with captured values if applicable
    if not invoke_shell:
        stdout_buf = ''.join(stdout_buf).strip()
        stderr_buf = ''.join(stderr_buf).strip()

    # Assemble output string
    out = AttributedString(stdout_buf)
    out.stderr = AttributedString(stderr_buf)
    out.command = command
    out.failed = False
    out.exit_status = status
    if status != 0:
        out.failed = True
        # raise SSHError(
        #     "execute() received nonzero return code %s while executing '%s' as '%s' on '%s'"
        #     % (status, command, user, host))
    out.succeeded = not out.failed
    return out


SORRY_TRY_AGAIN = 'Sorry, try again.'
SUDO_PROMPT = 'sudo password:'


def _endswith(char_list, substring):
    tail = char_list[-1 * len(substring):]
    substring = list(substring)
    return tail == substring


def _has_newline(bytelist):
    return '\r' in bytelist or '\n' in bytelist


def output_loop(user, host, chan, attr, stream, capture):
    ol = OutputLooper(user, host, chan, attr, stream, capture)
    ol.loop()


# TODO: replace with collections.deque(maxlen=xxx) in Python 2.6
class RingBuffer(list):
    def __init__(self, value, maxlen):
        super(RingBuffer, self).__init__(value)
        self._maxlen = maxlen

    def _free(self):
        return self._maxlen - len(self)

    def append(self, value):
        if self._free() == 0:
            del self[0]
        return super(RingBuffer, self).append(value)

    def extend(self, values):
        overage = len(values) - self._free()
        if overage > 0:
            del self[0:overage]
        return super(RingBuffer, self).extend(values)

    # Paranoia from here on out.
    def insert(self, index, value):
        raise ValueError("Can't insert into the middle of a ring buffer!")

    def __setslice__(self, i, j, sequence):
        raise ValueError("Can't set a slice of a ring buffer!")

    def __setitem__(self, key, value):
        if isinstance(key, slice):
            raise ValueError("Can't set a slice of a ring buffer!")
        else:
            return super(RingBuffer, self).__setitem__(key, value)


class OutputLooper(object):
    def __init__(self, user, host, chan, attr, stream, capture):
        self.user = user
        self.host = host
        self.chan = chan
        self.stream = stream
        self.capture = capture
        self.read_func = getattr(chan, attr)
        self.prefix = "[%s] %s: " % (host, "out" if attr == 'recv' else "err")
        self.printing = False
        self.linewise = True
        self.reprompt = False
        self.read_size = 4096
        self.write_buffer = RingBuffer([], maxlen=len(self.prefix))

    def _flush(self, text):
        self.stream.write(text)
        self.stream.flush()
        self.write_buffer.extend(text)

    def loop(self):
        """
        Loop, reading from <chan>.<attr>(), writing to <stream> and buffering to <capture>.
        """
        # Internal capture-buffer-like buffer, used solely for state keeping.
        # Unlike 'capture', nothing is ever purged from this.
        _buffer = []

        # Initialize loop variables
        initial_prefix_printed = False
        seen_cr = False
        line = []

        while True:
            # Handle actual read
            try:
                bytelist = self.read_func(self.read_size)
            except socket.timeout:
                continue
                # Empty byte == EOS
            if bytelist == '':
                # If linewise, ensure we flush any leftovers in the buffer.
                if self.linewise and line:
                    self._flush(self.prefix)
                    self._flush("".join(line))
                break
                # A None capture variable implies that we're in open_shell()
            if self.capture is None:
                # Just print directly -- no prefixes, no capturing, nada
                # And since we know we're using a pty in this mode, just go
                # straight to stdout.
                self._flush(bytelist)
            # Otherwise, we're in run/sudo and need to handle capturing and
            # prompts.
            else:
                # Print to user
                if self.printing:
                    printable_bytes = bytelist
                    # Small state machine to eat \n after \r
                    if printable_bytes[-1] == "\r":
                        seen_cr = True
                    if printable_bytes[0] == "\n" and seen_cr:
                        printable_bytes = printable_bytes[1:]
                        seen_cr = False

                    while _has_newline(printable_bytes) and printable_bytes != "":
                        # at most 1 split !
                        cr = re.search("(\r\n|\r|\n)", printable_bytes)
                        if cr is None:
                            break
                        end_of_line = printable_bytes[:cr.start(0)]
                        printable_bytes = printable_bytes[cr.end(0):]

                        if not initial_prefix_printed:
                            self._flush(self.prefix)

                        if _has_newline(end_of_line):
                            end_of_line = ''

                        if self.linewise:
                            self._flush("".join(line) + end_of_line + "\n")
                            line = []
                        else:
                            self._flush(end_of_line + "\n")
                        initial_prefix_printed = False

                    if self.linewise:
                        line += [printable_bytes]
                    else:
                        if not initial_prefix_printed:
                            self._flush(self.prefix)
                            initial_prefix_printed = True
                        self._flush(printable_bytes)

                # Now we have handled printing, handle interactivity
                read_lines = re.split(r"(\r|\n|\r\n)", bytelist)
                for fragment in read_lines:
                    # Store in capture buffer
                    self.capture += fragment
                    # Store in internal buffer
                    _buffer += fragment
                    # Handle prompts
                    prompt = _endswith(self.capture, SUDO_PROMPT)
                    try_again = (_endswith(self.capture, SORRY_TRY_AGAIN + '\n')
                                 or _endswith(self.capture, SORRY_TRY_AGAIN + '\r\n'))
                    if prompt:
                        self.prompt()
                    elif try_again:
                        self.try_again()

        # Print trailing new line if the last thing we printed was our line
        # prefix.
        if self.prefix and "".join(self.write_buffer) == self.prefix:
            self._flush('\n')

    def prompt(self):
        # Obtain cached password, if any
        password = get_password(self.user, self.host)
        # Remove the prompt itself from the capture buffer. This is
        # backwards compatible with Fabric 0.9.x behavior; the user
        # will still see the prompt on their screen (no way to avoid
        # this) but at least it won't clutter up the captured text.
        del self.capture[-1 * len(SUDO_PROMPT):]
        # If the password we just tried was bad, prompt the user again.
        if (not password) or self.reprompt:
            # Print the prompt and/or the "try again" notice if
            # output is being hidden. In other words, since we need
            # the user's input, they need to see why we're
            # prompting them.
            if not self.printing:
                self._flush(self.prefix)
                if self.reprompt:
                    self._flush(SORRY_TRY_AGAIN + '\n' + self.prefix)
                self._flush(SUDO_PROMPT)
                # Prompt for, and store, password. Give empty prompt so the
            # initial display "hides" just after the actually-displayed
            # prompt from the remote end.
            self.chan.input_enabled = False
            password = prompt_for_password(self.user, self.host, " ", self.stream)
            self.chan.input_enabled = True
            # Update env.password, env.passwords if necessary
            set_password(self.user, self.host, password)
            # Reset reprompt flag
            self.reprompt = False
            # Send current password down the pipe
        self.chan.sendall(password + '\n')

    def try_again(self):
        # Remove text from capture buffer
        self.capture = self.capture[:len(SORRY_TRY_AGAIN)]
        # Set state so we re-prompt the user at the next prompt.
        self.reprompt = True


def input_loop(chan, using_pty, debug=False):
    while not chan.exit_status_ready():
        r, w, x = select([sys.stdin], [], [], 0.0)
        have_char = (r and r[0] == sys.stdin)
        if have_char and chan.input_enabled:
            # Send all local stdin to remote end's stdin
            byte = sys.stdin.read(1)
            chan.sendall(byte)
            # Optionally echo locally, if needed.
            if not using_pty and debug:
                # Not using fastprint() here -- it prints as 'user' output level;
                # don't want it to be accidentally hidden
                sys.stdout.write(byte)
                sys.stdout.flush()
        time.sleep(paramiko.io_sleep)


def disconnect_all(parallel=True):
    def _close(host_string):
        console.debug("Disconnecting from %s" % host_string)
        connections[host_string].close()
        del connections[host_string]
        console.debug("Disconnected from %s" % host_string)

    call(connections.keys(), _close, parallel=parallel)
