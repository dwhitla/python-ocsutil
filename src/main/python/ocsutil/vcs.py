def read_branch_details():
    result = run_cmd("git branch -av", capture=True)
    branch_matcher = re.compile("^(\*?)\s*([a-zA-Z0-9_/]+)\s+([a-z0-9]{7})\s(.*)$")
    branch_details = {}
    for branch_detail in result.splitlines():
        if "->" in branch_detail:
            continue
        active, branch_name, latest_commit, commit_message = branch_matcher.match(branch_detail).groups()
        branch_details[branch_name] = (active == "*", latest_commit, commit_message)
    return branch_details


def git_status(basedir=None):
    if basedir is None:
        basedir = os.getcwd()
    result = run_cmd("git status -s", capture=True)
    delta_matcher = re.compile("^(..) (.+)$")
    untracked = list()
    added = list()
    modified = list()
    deleted = list()
    ignored = list()
    bins = {"A": added, "D": deleted, "M": modified, "?": untracked}
    for delta in result.splitlines():
        state, file_ = delta_matcher.match(delta).groups()
        if state[1] == ' ':
            staged = True
            state = state[0]
        else:
            staged = False
            state = state[1]
        bins.get(state, ignored).append(AttributedDict({"name": file_, "staged": staged}))
    return AttributedDict({
        "added": added,
        "deleted": deleted,
        "modified": modified,
        "untracked": untracked
    })


def interactive_merge(left, right, merged, base=None):
    if not base:
        base = left
    git_merge_tool = run_cmd("git config --get merge.tool", capture=True)
    if git_merge_tool and git_merge_tool.succeeded:
        token_repls = (
            (re.compile(r"\$LOCAL"), left),
            (re.compile(r"\$REMOTE"), right),
            (re.compile(r"\$MERGED"), merged),
            (re.compile(r"\$BASE"), base),
        )
        command = run_cmd("git config --get mergetool.%s.cmd" % git_merge_tool.strip(), capture=True)
        if command and command.succeeded:
            for token, repl in token_repls:
                command = token.sub(repl, command)
            return run_cmd(command, capture=True)
    warn("You cannot use the interactive merge feature until you have configured a \"mergetool\" in your ~/.gitconfig.")


