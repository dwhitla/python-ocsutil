import hashlib
from os import path
import os
import stat


def is_binary(openfile):
    CHUNKSIZE = 4096
    while True:
        chunk = openfile.read(CHUNKSIZE)
        if '\0' in chunk:
            return True
        if len(chunk) < CHUNKSIZE:
            break
    return False


def calculate_md5sum(filename):
    with open(filename, 'rb') as f:
        md5 = hashlib.md5()
        while True:
            data = f.read(8192)
            if not data:
                break
            md5.update(data)
        return md5.hexdigest()


# path stuff

def make_relative_link(rootdir, frompath, topath):
    if not topath.startswith("/"):
        return topath
    rootdir = path.abspath(rootdir)
    frompath = path.normpath(path.join(rootdir, frompath))
    topath = path.normpath(path.join(rootdir, topath[1:]))
    return path.relpath(topath, path.dirname(frompath))


def antescendants_of(a_file):
    s = set()
    parent = a_file
    while True:
        parent = path.dirname(parent)
        if not parent:
            break
        s.add(parent)
    return s


#noinspection PyBroadException
def descendants_of(rootdir, relative_to=None, excludes=None, prune_empty_directories=False, include_directories=True):
    if relative_to is None:
        relative_to = rootdir
    if excludes is None:
        excludes = []
    descendants = []
    children = os.listdir(rootdir)
    for child in children:
        if child in excludes:
            continue
        child_path = path.join(rootdir, child)
        child_rel_path = path.relpath(child_path, relative_to)
        descendants.append(child_rel_path)
        try:
            st = os.lstat(child_path)
            if stat.S_ISDIR(st.st_mode):
                grandchildren = descendants_of(
                    child_path, relative_to, excludes, prune_empty_directories, include_directories
                )
                if not include_directories or (not grandchildren and prune_empty_directories):
                    descendants.pop()
                descendants.extend(grandchildren)
        except:
            continue
    return descendants


