import sys
from Queue import Queue
from threading import Thread


class ThreadHandler(Thread):

    def __init__(self, func, *args, **kwargs):
        def wrapper(*args, **kwargs):
            try:
                self._result = func(*args, **kwargs)
            except BaseException:
                self.exception = sys.exc_info()

        super(ThreadHandler, self).__init__(target=wrapper, args=args, kwargs=kwargs)
        self._result = None
        self.exception = None
        self.daemon = True
        self.start()

    def raise_if_needed(self):
        if self.exception:
            e = self.exception
            raise e[0], e[1], e[2]

    def result(self, timeout=None):
        self.join(timeout)
        return self._result


class Worker(Thread):

    def __init__(self, tasks, continue_on_error=False):
        """
        :type tasks: Queue
        :type continue_on_error: bool
        """
        Thread.__init__(self)
        self.exception = None
        self.tasks = tasks
        self.daemon = True
        self.continue_on_error = continue_on_error
        self.start()

    def run(self):
        running = True
        while running:
            func, callback, args, kwargs = self.tasks.get()
            exception = None
            try:
                func(*args, **kwargs)
            except BaseException, e:
                exception = sys.exc_info()
                if not self.continue_on_error:
                    self.exception = exception
                    running = False
            finally:
                self.tasks.task_done()
                if callable(callback):
                    try:
                        callback(error=exception)
                    except:
                        pass

    def raise_if_needed(self):
        if self.exception:
            e = self.exception
            raise e[0], e[1], e[2]


class ThreadPool(object):

    def __init__(self, pool_size):
        super(ThreadPool, self).__init__()
        self.tasks = Queue()
        self.workers = set()
        for _ in range(pool_size):
            self.workers.add(Worker(self.tasks))

    def add_task(self, func, callback=None, *args, **kwargs):
        self.tasks.put((func, callback, args, kwargs))

    def await_completion(self):
        self.tasks.join()
        for worker in self.workers:
            worker.raise_if_needed()
