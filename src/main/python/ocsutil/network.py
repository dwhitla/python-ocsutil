import socket
import dns.name


def local_domain():
    hostname = dns.name.from_text(socket.getfqdn())
    return hostname.parent().to_text(omit_final_dot=False)


def get_source_ip(destination):
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        sock.connect((destination, 7))
        source = sock.getsockname()[0]
    except:
        source = socket.gethostbyname(socket.getfqdn())
    finally:
        sock.close()
    return source


def host_answers(host, port=22, timeout=1):
    for res in socket.getaddrinfo(host, port, 0, socket.SOCK_STREAM):
        addr_family, socktype, proto, canonname, sa = res
        sock = None
        try:
            sock = socket.socket(addr_family, socktype, proto)
            sock.settimeout(timeout)
            sock.connect(sa)
            return True
        except:
            return False

        finally:
            if sock:
                sock.close()
