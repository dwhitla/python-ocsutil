import StringIO
import argparse
import collections
from contextlib import contextmanager
import os
import pydoc
import re
import sys
import itertools
from abc import ABCMeta, abstractmethod
from blessings import Terminal
import time

__version__ = '1.0'
__all__ = [
    'FG',
    'yes_no',
    'choice',
    'yes_no_other',
    'move_cursor',
    'debug',
    'warn',
    'error',
    'abort',
]


class Command:
    def __init__(self):
        pass

    __metaclass__ = ABCMeta

    @classmethod
    def configure_parser(cls, parser):
        return NotImplemented

    @abstractmethod
    def execute(self, *args, **kwargs):
        raise NotImplementedError


term = Terminal()


# user input output
class ANSI:
    BOLD_ON = "\033[1m" if term.is_a_tty else ""
    BOLD_OFF = "\033[21m" if term.is_a_tty else ""
    RESET = "\033[0m" if term.is_a_tty else ""
    UNDERLINE = "\033[4m" if term.is_a_tty else ""
    INVERT = "\033[7m" if term.is_a_tty else ""

    @staticmethod
    def FG_colour(code):
        return "\033[38;5;%im" % (code % 256) if term.is_a_tty else ""

    @staticmethod
    def BG_colour(code):
        return "\033[48;5;%im" % (code % 256) if term.is_a_tty else ""

    @staticmethod
    def e(format_sequence, *args):
        _output = ''.join(args)
        if term.is_a_tty:
            _output = str(format_sequence) + _output + ANSI.RESET
        return _output

    class FG:
        BLUE = '\033[94m' if term.is_a_tty else ''
        YELLOW = '\033[93m' if term.is_a_tty else ''
        GREEN = '\033[92m' if term.is_a_tty else ''
        RED = '\033[91m' if term.is_a_tty else ''
        BLACK = '\033[30m' if term.is_a_tty else ''
        WHITE = '\033[97m' if term.is_a_tty else ''
        LIGHT_GRAY = '\033[37m' if term.is_a_tty else ''
        DARK_GRAY = '\033[90m' if term.is_a_tty else ''
        DEFAULT = '\033[39m' if term.is_a_tty else ''

    class BG:
        BLUE = '\033[104m' if term.is_a_tty else ''
        YELLOW = '\033[103m' if term.is_a_tty else ''
        GREEN = '\033[102m' if term.is_a_tty else ''
        RED = '\033[101m' if term.is_a_tty else ''
        BLACK = '\033[40m' if term.is_a_tty else ''
        WHITE = '\033[107m' if term.is_a_tty else ''
        LIGHT_GRAY = '\033[47m' if term.is_a_tty else ''
        DARK_GRAY = '\033[100m' if term.is_a_tty else ''
        DEFAULT = '\033[49m' if term.is_a_tty else ''


def _print(*args, **kwargs):
    out = kwargs.get('file', sys.stdout)
    sep = kwargs.get('sep', '')
    end = kwargs.get('end', '\n')
    out.write(sep.join(args))
    out.write(end)


def move_cursor(row=None, column=None):
    if row is None:
        return "\033[H" if column is None else "\033[%iG" % column
    return "\033[%iH" % row if column is None else "\033[%i;%if" % (row, column)


def termsize():
    """Try to figure out the size of the current terminal.

    Returns:
        Size of the terminal as a tuple: (height, width).
    """
    env = os.environ

    def ioctl_GWINSZ(fd):
        try:
            import fcntl
            import termios
            import struct
            cr = struct.unpack('hh', fcntl.ioctl(fd, termios.TIOCGWINSZ, '1234'))
        except:
            return None
        return cr

    cr = ioctl_GWINSZ(0) or ioctl_GWINSZ(1) or ioctl_GWINSZ(2)
    if not cr:
        try:
            fd = os.open(os.ctermid(), os.O_RDONLY)
            cr = ioctl_GWINSZ(fd)
            os.close(fd)
        except:
            pass
    if not cr:
        try:
            cr = (env['LINES'], env['COLUMNS'])
        except:
            cr = (25, 80)
    return int(cr[1]), int(cr[0])


class DisplayField(object):

    def __init__(self, label, value, width=0, justify=None):
        super(DisplayField, self).__init__()
        self.label = label
        self.value = value
        self.justify = justify
        if width:
            self.width = width
        else:
            self.width = 0 if callable(label) else len(label)

    def __repr__(self):
        return 'display_field(label=%r, value=%r, width=%r)' % self


def display_table(resultset, columns, out=sys.stdout, max_fieldsize=100,
                  order_by=None, group_by=None, group_headings=False):
    """Render an ascii-table"""
    default_fg_colour = ANSI.FG_colour(248)
    default_bg_colour = ANSI.BG_colour(0)
    odd_fg_colour = ANSI.FG_colour(241)
    odd_bg_colour = ANSI.BG_colour(0)
    even_fg_colour = ANSI.FG_colour(246)
    even_bg_colour = ANSI.BG_colour(233)
    heading_fg_colour = ANSI.FG_colour(166)

    def println(*args, **kwargs):
        out.write(ANSI.e(default_fg_colour + ANSI.BG_colour(0), ''.join(args)))
        out.write('\n')

    def print_horizontal_line():
        println('+', '-'.join(('-' * (c.width + 2) for c in columns)), '+')

    def print_headings():
        print_horizontal_line()
        println('|',
                '|'.join((heading_fg_colour + ' %-*s ' % (c.width, c.label) + default_fg_colour) for c in columns),
                '|')
        print_horizontal_line()

    def print_group_header(group):
        x = sum(((c.width + 3) for c in columns)) + 1
        label = evaluate(group, group_by.label)
        if group_headings:
            println(ANSI.FG_colour(241), "\033[4m" + "_" * x)
        _print(ANSI.BG_colour(241), ANSI.FG_colour(232), ANSI.UNDERLINE if group_headings else '', ' ' * x,
               move_cursor(column=(x - len(label)) / 2), ANSI.BOLD_ON, label, ANSI.BOLD_OFF,
               ANSI.RESET, file=out)
        if group_headings:
            print_headings()
        else:
            print_horizontal_line()

    def isublists(l, n):
        return itertools.izip_longest(*[iter(l)] * n)

    def trim(value, max_fieldsize):
        value = str(value)
        if len(value) > max_fieldsize:
            value = value[:max_fieldsize - 5] + '[...]'
        value = value.replace('\n', '^').replace('\r', '^').replace('\t', ' ')
        return value

    def evaluate(result, value):
        return value(result) if callable(value) else value

    groups = {}
    for result in resultset:
        if group_by:
            group_results = groups.setdefault(evaluate(result, group_by.value), [])
        else:
            group_results = groups.setdefault(None, [])
        column_values = {}
        for column in columns:
            value = trim(str(evaluate(result, column.value)), max_fieldsize)
            column_values[column] = value
            column.width = max(column.width, len(value))
        order_key = evaluate(result, order_by) if order_by else None
        group_results.append((order_key, column_values))

    screen_x, screen_y = termsize()
    paginate = len(resultset) > screen_y - 4
    # if paginate:
    #     resultset = isublists(resultset, screen_y - 3)
    # else:
    #     resultset = isublists(resultset, resultset.length)

    odd = True
    if not group_headings:
        print_headings()
    for group in sorted(groups.keys()):
        if group_headings:
            odd = True
        if group_by:
            print_group_header(group)
        group_results = groups[group]
        for _, column_values in sorted(group_results) if order_by else group_results:
            if odd:
                row_fg_colour = odd_fg_colour
                row_bg_colour = odd_bg_colour
            else:
                row_fg_colour = even_fg_colour
                row_bg_colour = even_bg_colour
            println('|', row_fg_colour, row_bg_colour,
                    '|'.join(
                        ((' %*s ' if c.justify == 'r' else ' %-*s ') % (c.width, column_values[c]) for c in columns)
                    ),
                    default_fg_colour, default_bg_colour, '|', file=out)
            odd = not odd
        print_horizontal_line()
    out.write('\n')


def yes_no(question, default="n"):
    """ Returns True for yes or False for no. """
    default = "y" if default.startswith("y") else "n"
    while True:
        answer = raw_input("%s? ([%s]es|[%s]o)" % (
            question,
            'Y' if default == 'y' else 'y',
            'N' if default == 'n' else 'n')).lower()
        if not answer:
            return default == 'y'
        if answer[0] in ['y', 'n']:
            return answer[0] == 'y'


def choice(question, options=None, keys=None, default=None):
    prompt = None
    choices = collections.OrderedDict()
    default_key = str(default)[0].lower() if default else None
    if keys:
        # ensure keys and options are the same length
        if len(keys) != len(options):
            raise Exception("There must be as many keys as options")
    else:
        keys = options
    for k, v in zip(keys, options):
        choice_key = str(k)[0].lower()
        choice = str(v)
        if not (choice_key in choices or choice in choices.values()):
            choices[choice_key] = choice
            choice_display = re.sub(
                choice_key,
                "[%s]" % (choice_key.upper() if choice_key == default_key else choice_key),
                choice.lower(), 1)
            prompt = "%s|%s" % (prompt, choice_display) if prompt else choice_display

    prompt = "%s? (%s)" % (question, prompt)
    while True:
        choice = raw_input(prompt)
        if not choice:
            return default
        if choice in choices:
            return choices.get(choice)


def yes_no_other(question, other, default=''):
    """ Return 'y' for yes, 'n' for no or other[0] for other"""
    other = other.lower()
    option = other[0]
    if default and default[0] in ['y', 'n', option]:
        default = default[0]
    else:
        default = 'n'
    while True:
        answer = raw_input("%s? ([%s]es|[%s]o|[%s]%s)" % (
            question,
            'Y' if default == 'y' else 'y',
            'N' if default == 'n' else 'n',
            option.upper() if default == option else option, other[1:]
        )).lower()
        if not answer:
            return default
        if answer[0] in ['y', 'n', option]:
            return answer[0]


_debug = False


def enable_debug(flag=True):
    global _debug
    _debug = flag


def debug(message, end='\n'):
    global _debug
    if _debug:
        _print(ANSI.FG.DARK_GRAY, message, ANSI.FG.DEFAULT, end=end)


def info(message, end='\n'):
    _print(ANSI.FG.DARK_GRAY, message, ANSI.FG.DEFAULT, end=end)


def warn(message, detail=None, end='\n', out=sys.stderr):
    _print(ANSI.FG.YELLOW, message, ANSI.FG.DEFAULT, file=out, end=end)
    if detail:
        _print(detail, file=sys.stderr)


def error(message, detail=None, out=sys.stderr):
    _print(ANSI.FG.RED, message, ANSI.FG.DEFAULT, file=out)
    if detail:
        _print(detail, file=sys.stderr)


def abort(message, detail=None, runhook=None, args=None):
    error(message, detail)
    if args is None:
        args = []
    if runhook:
        runhook(*args)
    exit(1)


def ls_diff(old, new):
    def markup(key, value):
        return (ANSI.BOLD_ON + value + ANSI.BOLD_OFF) if (key in changed) else value

    changed = []
    if new.uname != old.uname:
        changed.append("uname")
    if new.gname != old.gname:
        changed.append("gname")
    if new.mode != old.mode or new.type != old.type:
        changed.append("permissions")
    if new.mtime != old.mtime:
        changed.append("mtime")
    if new.size != old.size:
        changed.append("size")

    uname_len = max(len(old.uname), len(new.uname))
    gname_len = max(len(old.gname), len(new.gname))
    path_info_len = max(len(old.path_info()), len(new.path_info()))
    print(ANSI.FG.RED + "- %s  %s  %s  %s  %s  %s%s<from %s>" % (
        markup("permissions", old.permissions()),
        markup("uname", "%-*s" % (uname_len, old.uname)),
        markup("gname", "%-*s" % (gname_len, old.gname)),
        markup("size", "%6i" % old.size),
        markup("mtime", old.last_modified()),
        "%-*s" % (path_info_len, old.path_info()),
        move_cursor(column=70),
        old.ucsinfo
    ) + ANSI.FG.DEFAULT)
    print(ANSI.FG.GREEN + "+ %s  %s  %s  %s  %s  %s%s<from %s>" % (
        markup("permissions", new.permissions()),
        markup("uname", "%-*s" % (uname_len, new.uname)),
        markup("gname", "%-*s" % (gname_len, new.gname)),
        markup("size", "%6i" % new.size),
        markup("mtime", new.last_modified()),
        "%-*s" % (path_info_len, new.path_info()),
        move_cursor(column=70),
        new.ucsinfo
    ) + ANSI.FG.DEFAULT)


def colour_diff(diff_output):
    def unescape_line(s):
        return re.compile(r"\\").sub(r"\\\\", s.rstrip()) + "\n"

    colours = {
        re.compile(r"^\+[^\+]"): ANSI.FG.GREEN + r"%s" + ANSI.FG.DEFAULT,
        re.compile(r"^-[^-]"): ANSI.FG.RED + r"%s" + ANSI.FG.DEFAULT,
        re.compile(r"^(\+\+\+ |--- )"): ANSI.FG.BLUE + r"%s" + ANSI.FG.DEFAULT,
        re.compile(r"^@@ "): ANSI.FG.YELLOW + r"%s" + ANSI.FG.DEFAULT,
    }
    coloured_output = ""
    for line in diff_output:
        for pattern, replacement in colours.iteritems():
            match = pattern.match(line)
            if match:
                line = match.expand(replacement % unescape_line(line))
                break
        coloured_output += line
    pydoc.pipepager(coloured_output, cmd='less -R')


class ArgumentParser(argparse.ArgumentParser):

    def __init__(self, version=None, add_help=True, **kwargs):
        _kwargs = {
            "formatter_class": argparse.RawTextHelpFormatter
        }
        _kwargs.update(kwargs)
        super(ArgumentParser, self).__init__(add_help=False, **_kwargs)
        if add_help:
            self.add_argument("-h", "--help", action='help', default=argparse.SUPPRESS,
                              help="Print this help message and exit.")
        if version:
            self.add_argument('-V', "--version", action="version", default=argparse.SUPPRESS, version=version,
                              help="Print the program's version number and exit.")

    def error(self, message):
        detail = StringIO.StringIO()
        self.print_help(detail)
        abort("Error: " + message, detail.getvalue())


@contextmanager
def timer(name=None):
    name = name + ": " if name else ''
    start = time.clock()
    try:
        yield
    finally:
        finish = time.clock()
        debug("%s%d ms" % (name, (finish - start) * 1000))
