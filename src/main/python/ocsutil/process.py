#noinspection PyBroadException
from contextlib import contextmanager
import os
import signal
import subprocess
import sys
from ocsutil import console
from ocsutil.console import error


class CommandResult(object):

    def __init__(self, code=0, stdout='', stderr=''):
        super(CommandResult, self).__init__()
        self.code = code
        self.stdout = stdout
        self.stderr = stderr

    @property
    def __repr__(self):
        return self.stdout

    @property
    def __str__(self):
        return self.stdout

    @property
    def succeeded(self):
        return self.code == 0

    @property
    def failed(self):
        return self.code != 0


def run_cmd(command, capture=False, debug=False):
    dev_null = None
    if capture:
        out_stream = subprocess.PIPE
        err_stream = subprocess.PIPE
    else:
        dev_null = open(os.devnull, 'w+')
        out_stream = dev_null
        err_stream = dev_null
    try:
        if debug:
            console.debug(command)
        p = subprocess.Popen(command, shell=True, stdout=out_stream, stderr=err_stream)
        (stdout, stderr) = p.communicate()
        result = CommandResult(p.returncode, stdout, stderr)
    finally:
        if dev_null is not None:
            dev_null.close()
    return result


@contextmanager
def exit_handlers(handlers=None, cleanup_calls=None, show_trace=False):
    def _handler_runner(actions):
        _actions = [actions]

        #noinspection PyBroadException
        def _run():
            exc_info = None
            while _actions:
                kargs = {}
                pargs = []
                action = _actions.pop()
                try:
                    func = action[0]
                    pargs = action[1]
                    kargs = action[2]
                except TypeError:
                    func = action
                except IndexError:
                    pass
                try:
                    func(*pargs, **kargs)
                except SystemExit:
                    exc_info = sys.exc_info()
                except:
                    import traceback
                    print >> sys.stderr, "Error in exit_handler:%s:" % func
                    traceback.print_exc()
                    exc_info = sys.exc_info()
            if exc_info is not None:
                raise exc_info[0], exc_info[1], exc_info[2]

        return _run

    if handlers is None:
        handlers = {}
    if type(handlers) != dict:
        handlers = {signal.SIGABRT: handlers}
    for sig, actions in handlers.iteritems():
        signal.signal(sig, _handler_runner(actions))
    exit_code = 0
    try:
        yield
    except Exception, _error:
        detail = None
        if type(_error.message) == dict:
            message = _error.message.pop('desc', '')
            detail = "\n".join(("%s: %s" % (k, v) for (k, v) in _error.message.iteritems()))
        else:
            message = _error.message
            if hasattr(_error, "cause"):
                detail = _error.cause
            elif hasattr(_error, "detail"):
                detail = _error.detail
        error("Error: %s" % message, detail)
        if show_trace:
            import traceback
            traceback.print_exc()
        exit_code = 1
    finally:
        if cleanup_calls:
            if not hasattr(cleanup_calls, "__iter__"):
                cleanup_calls = [cleanup_calls]
            for cleanup_call in cleanup_calls:
                try:
                    cleanup_call()
                except:
                    pass
        # reset the signal defaults before exit
        for sig in handlers.keys():
            signal.signal(sig, signal.SIG_DFL)
        exit(exit_code)


class call(object):
    def __init__(self, func, *args, **kwargs):
        super(call, self).__init__()
        if not callable(func):
            raise TypeError
        self._callable = func
        self._args = args
        self._kwargs = kwargs

    def __call__(self, *args, **kwargs):
        cargs = list(self._args)
        cargs.extend(args)
        ckwargs = self._kwargs.copy()
        ckwargs.update(kwargs)
        self._callable(*cargs, **ckwargs)
