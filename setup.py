#!/usr/bin/env python
from setuptools import setup, find_packages
import os
import sys


if sys.platform == 'darwin' and [int(x) for x in os.uname()[2].split('.')] >= [11, 0, 0]:
    # clang is serious now
    extra_compile_args = ['-Werror', '-Wunused-command-line-argument-hard-error-in-future', '-Qunused-arguments']
else:
    extra_compile_args = ['-Werror']


setup(
    name="ocsutil",
    version="1.0.5",
    description="",
    package_dir={'': "src/main/python"},
    packages=find_packages("src/main/python"),
    package_data={'': ['*.conf']},
    install_requires=[
        "blessings==1.5.1",
        "pycrypto",
        "paramiko==2.0.1",
        "dnspython==1.11.0",
    ],
    url='',
    license='',
    author="dwhitla",
    author_email="dave.whitla@ocean.net.au",
    classifiers=[
        "Programming Language :: Python :: 2",
        "Development Status :: 3 - Alpha",
        "Topic :: Software Development :: Libraries :: Python Modules",
        "Topic :: System :: Systems Administration :: Authentication/Directory"
    ],
)
